package com.example.andreonis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EnigmaInterviewTasksApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnigmaInterviewTasksApplication.class, args);
	}
}
