package com.example.andreonis.thirdTaskInvoices.web.controllers;

import com.example.andreonis.thirdTaskInvoices.services.interfaces.InvoicesService;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.param.InvoiceCreateParams;
import com.stripe.param.PriceCreateParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/invoices")
@RequiredArgsConstructor
@Slf4j
public class InvoicesController {

    private final InvoicesService invoicesService;

    @GetMapping
    public List<Invoice> invoices() throws StripeException {
        return invoicesService.getAllInvoices();
    }

    @GetMapping("/prices")
    public List<Price> prices() throws StripeException {
        return invoicesService.getAllPrices();
    }

    @GetMapping("/customers")
    public List<Customer> customers() throws StripeException {
        return invoicesService.getAllCustomers();
    }


    @GetMapping("/invoicePDF/{invoiceId}")
    public RedirectView invoicePDF(@PathVariable String invoiceId) throws StripeException, IOException {
        Invoice invoice = Invoice.retrieve(invoiceId);
        return new RedirectView(invoicesService.downloadInvoicePdf(invoice));
    }

    @PostMapping("/product/{name}")
    @ResponseStatus(HttpStatus.CREATED)
    public Product product(@PathVariable String name) throws StripeException {
        return invoicesService.createNewProduct(name);
    }

    @PostMapping("/price")
    @ResponseStatus(HttpStatus.CREATED)
    public Price price(@RequestParam String productId,
                             @RequestParam @NotNull String interval,
                             @RequestParam Long unitAmount,
                             @RequestParam String currency) throws StripeException {
        Product product = Product.retrieve(productId);
        PriceCreateParams.Recurring recurring = invoicesService.createRecurring(PriceCreateParams.Recurring.Interval.valueOf(interval.toUpperCase()));
        return invoicesService.createNewPrice(product, unitAmount, currency, recurring);
    }

    @PostMapping("/customer")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer customer(@RequestParam String nameAndSurname,
                                  @RequestParam String email,
                                  @RequestParam String description) throws StripeException {
        return invoicesService.createNewCustomer(nameAndSurname, email, description);
    }

    @PostMapping("/invoiceItem")
    @ResponseStatus(HttpStatus.CREATED)
    public InvoiceItem invoiceItem(@RequestParam String customerId,
                                         @RequestParam String priceId) throws StripeException {
        Customer customer = Customer.retrieve(customerId);
        Price price = Price.retrieve(priceId);
        return invoicesService.createNewInvoiceItem(customer, price);
    }

    @PostMapping("/invoice")
    @ResponseStatus(HttpStatus.CREATED)
    public Invoice invoice(@RequestParam String customerId,
                                 @RequestParam boolean setAutoAdvance,
                                 @RequestParam String collectionMethodType) throws StripeException {
        Customer customer = Customer.retrieve(customerId);
        return invoicesService.createNewInvoice(customer, setAutoAdvance, InvoiceCreateParams.CollectionMethod.valueOf(collectionMethodType.toUpperCase()));
    }
}
