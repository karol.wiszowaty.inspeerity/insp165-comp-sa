package com.example.andreonis.thirdTaskInvoices.services;

import com.example.andreonis.thirdTaskInvoices.services.interfaces.InvoicesService;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.param.*;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
public class InvoicesServiceImpl implements InvoicesService {

    public List<Invoice> getAllInvoices() throws StripeException {
        InvoiceCollection invoice = Invoice.list(InvoiceListParams.builder().build());
        return invoice.getData();
    }

    public List<Customer> getAllCustomers() throws StripeException {
        CustomerCollection customerCollection = Customer.list(CustomerListParams.builder().build());
        return customerCollection.getData();
    }

    public List<Price> getAllPrices() throws StripeException {
        PriceCollection priceCollection = Price.list(PriceListParams.builder().build());
        return priceCollection.getData();
    }

    public Product createNewProduct(String name) throws StripeException {
        ProductCreateParams productParams =
                ProductCreateParams.builder()
                        .setName(name)
                        .build();
        return Product.create(productParams);
    }

    public Price createNewPrice(Product product, Long unitAmount, String currency, PriceCreateParams.Recurring recurring) throws StripeException {
        PriceCreateParams priceParams =
                PriceCreateParams.builder()
                        .setProduct(product.getId())
                        .setUnitAmount(unitAmount)
                        .setCurrency(currency)
                        .setRecurring(recurring)
                        .build();
        return Price.create(priceParams);
    }

    public PriceCreateParams.Recurring createRecurring(PriceCreateParams.Recurring.Interval interval) {
        return PriceCreateParams.Recurring.builder()
                .setInterval(interval)
                .build();
    }

    public Customer createNewCustomer(String nameAndSurname, String email, String description) throws StripeException {
        CustomerCreateParams customerParams =
                CustomerCreateParams.builder()
                        .setName(nameAndSurname)
                        .setEmail(email)
                        .setDescription(description)
                        .build();

        return Customer.create(customerParams);
    }

    public InvoiceItem createNewInvoiceItem(Customer customer, Price price) throws StripeException {
        InvoiceItemCreateParams invoiceItemParams =
                InvoiceItemCreateParams.builder()
                        .setCustomer(customer.getId())
                        .setPrice(price.getId())
                        .build();

        return InvoiceItem.create(invoiceItemParams);
    }

    public Invoice createNewInvoice(Customer customer, boolean setAutoAdvance,
                                    InvoiceCreateParams.CollectionMethod collectionMethodType) throws StripeException {
        InvoiceCreateParams invoiceParams =
                InvoiceCreateParams.builder()
                        .setCustomer(customer.getId())
                        .setAutoAdvance(setAutoAdvance) // Auto-finalize this draft after ~1 hour
                        .setCollectionMethod(collectionMethodType)
                        .build();

        return Invoice.create(invoiceParams);
    }

    public String downloadInvoicePdf(Invoice invoice) {
//        System.getProperty("user.dir");
//        InputStream in = new URL(Invoice.retrieve(invoice.getId()).getInvoicePdf()).openStream();
//        InputStreamResource resource = new InputStreamResource(in);
//		Files.copy(in, Paths.get(System.getProperty("user.dir") + "\\src\\resources\\downloads\\Invoice-"+ invoice.getNumber() + ".pdf"), StandardCopyOption.REPLACE_EXISTING);
        return /*"redirect://" + */invoice.getInvoicePdf();
    }
}
