package com.example.andreonis.thirdTaskInvoices.services.interfaces;

import com.stripe.exception.StripeException;
import com.stripe.model.*;
import com.stripe.param.InvoiceCreateParams;
import com.stripe.param.PriceCreateParams;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface InvoicesService {

    List<Invoice> getAllInvoices() throws StripeException;

    List<Customer> getAllCustomers() throws StripeException;

    List<Price> getAllPrices() throws StripeException;

    Product createNewProduct(String name) throws StripeException;

    Price createNewPrice(Product product, Long unitAmount, String currency, PriceCreateParams.Recurring recurring) throws StripeException;

    PriceCreateParams.Recurring createRecurring(PriceCreateParams.Recurring.Interval interval);

    Customer createNewCustomer(String nameAndSurname, String email, String description) throws StripeException;

    InvoiceItem createNewInvoiceItem(Customer customer, Price price) throws StripeException;

    Invoice createNewInvoice(Customer customer, boolean setAutoAdvance, InvoiceCreateParams.CollectionMethod collectionMethodType) throws StripeException;

    String downloadInvoicePdf(Invoice invoice) throws StripeException, IOException;

}
