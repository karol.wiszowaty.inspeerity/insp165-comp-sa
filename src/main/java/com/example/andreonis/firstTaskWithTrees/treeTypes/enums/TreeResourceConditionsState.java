package com.example.andreonis.firstTaskWithTrees.treeTypes.enums;

public enum TreeResourceConditionsState {
    GREAT,
    AVERAGE,
    LOW,
    NOT_ENOUGH
}
