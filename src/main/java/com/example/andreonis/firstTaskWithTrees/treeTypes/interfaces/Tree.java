package com.example.andreonis.firstTaskWithTrees.treeTypes.interfaces;

import com.example.andreonis.firstTaskWithTrees.treeTypes.enums.TreeResourceConditionsState;

public interface Tree {
    void grow();
    default void wilt(){
            System.out.println("Process of wilting started...");
    }
    default void feedLeaves(TreeResourceConditionsState resourceConditionsState) throws RuntimeException{
        if (resourceConditionsState == TreeResourceConditionsState.NOT_ENOUGH) {
            throw new RuntimeException("Not enough resources to start process of feeding up!");
        }
        System.out.println("Feeding leaves...");
    }
}
