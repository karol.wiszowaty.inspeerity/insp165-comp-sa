package com.example.andreonis.firstTaskWithTrees.treeTypes.treeSubTypes;

import com.example.andreonis.firstTaskWithTrees.treeTypes.interfaces.Tree;
import com.example.andreonis.firstTaskWithTrees.treeTypes.enums.TreeResourceConditionsState;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public abstract class LeafTree extends TreeBase {
    private String leafType = "Leaves";
    private String leafShape = "Wide";

    @Override
    public void grow() {
        log.info("Leaf type tree growing by 5cm...");
    }

    public abstract void dropLeaves();
}
