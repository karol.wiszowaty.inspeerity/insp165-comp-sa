package com.example.andreonis.firstTaskWithTrees.treeTypes.treeSubTypes;

import com.example.andreonis.firstTaskWithTrees.treeTypes.interfaces.Tree;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public abstract class TreeBase implements Tree {
    private String leafType = "SomeLeafType";
    private String leafShape = "SomeLeafShape";
    private int amountOfBranches = 0;
    private String trunk = "SomeTrunkType";

    @Override
    public void grow() {
        log.info("Default amount of growing which is 1cm");
    }
}
