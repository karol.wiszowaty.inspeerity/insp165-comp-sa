package com.example.andreonis.firstTaskWithTrees.treeTypes.services;

import com.example.andreonis.firstTaskWithTrees.treeTypes.treeSubTypes.LeafTree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Data
@EqualsAndHashCode(callSuper = false)
public class CherryTree extends LeafTree {
    public CherryTree() {
        this.setLeafType("glossy green leaves with toothed edges");
        this.setAmountOfBranches(10);
    }

    @Override
    public void grow() {
        log.info("Cherry tree that is subtype of leaf type tree growing by 8cm...");
    }

    @Override
    public void dropLeaves() {
        log.info("Cherry tree that is subtype of leaf type tree dropping 10 leaves...");
    }
}
