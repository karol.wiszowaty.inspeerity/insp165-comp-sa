package com.example.andreonis.firstTaskWithTrees.treeTypes.services;

import com.example.andreonis.firstTaskWithTrees.treeTypes.treeSubTypes.ConiferTree;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Data
@Slf4j
@EqualsAndHashCode(callSuper = false)
public class WeepingTree extends ConiferTree {
    @Override
    public void dropNeedles() {
        log.info("Weeping tree that is subtype of conifer type tree dropping 100 needles...");
    }
}
