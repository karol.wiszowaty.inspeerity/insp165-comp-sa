package com.example.andreonis.firstTaskWithTrees.treeTypes.treeSubTypes;

import com.example.andreonis.firstTaskWithTrees.treeTypes.interfaces.Tree;
import com.example.andreonis.firstTaskWithTrees.treeTypes.enums.TreeResourceConditionsState;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public abstract class ConiferTree extends TreeBase {

    @Override
    public void grow() {
        log.info("Conifer type tree growing by 2cm...");
    }

    public abstract void dropNeedles();
}
