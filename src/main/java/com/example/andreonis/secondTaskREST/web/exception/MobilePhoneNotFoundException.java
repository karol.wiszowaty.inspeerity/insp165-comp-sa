package com.example.andreonis.secondTaskREST.web.exception;

public class MobilePhoneNotFoundException extends RuntimeException{
    public MobilePhoneNotFoundException() {
    }

    public MobilePhoneNotFoundException(String message) {
        super(message);
    }

    public MobilePhoneNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public MobilePhoneNotFoundException(Throwable cause) {
        super(cause);
    }

    public MobilePhoneNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
