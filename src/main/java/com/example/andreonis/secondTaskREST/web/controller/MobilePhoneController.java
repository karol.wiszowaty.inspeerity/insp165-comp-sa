package com.example.andreonis.secondTaskREST.web.controller;

import com.example.andreonis.secondTaskREST.persistence.model.MobilePhone;
import com.example.andreonis.secondTaskREST.persistence.repo.MobilePhoneRepository;
import com.example.andreonis.secondTaskREST.web.exception.MobilePhoneNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/mobilephones")
@RequiredArgsConstructor
public class MobilePhoneController {


    private final MobilePhoneRepository mobilePhoneRepository;

    @GetMapping
    public Iterable<MobilePhone> findAll() {
        return mobilePhoneRepository.findAll();
    }

    @GetMapping("/{deviceId}")
    public MobilePhone findOne(@PathVariable String deviceId) {
        return mobilePhoneRepository.findByDeviceId(deviceId)
                .orElseThrow(MobilePhoneNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MobilePhone create(@RequestBody MobilePhone mobilePhone) {
        return mobilePhoneRepository.save(mobilePhone);
    }

    @DeleteMapping("/{deviceId}")
    public void delete(@PathVariable String deviceId) {
        MobilePhone result = mobilePhoneRepository.findByDeviceId(deviceId)
                .orElseThrow(MobilePhoneNotFoundException::new);
        mobilePhoneRepository.deleteById(result.getId());
    }

    @PutMapping("/{deviceId}")
    public MobilePhone updateBook(@RequestBody MobilePhone mobilePhone, @PathVariable String deviceId) {
        if (!mobilePhone.getDeviceId().equals(deviceId)) {
            throw new RuntimeException("Mobile phone device ids mismatch exception");
        }
        MobilePhone result = mobilePhoneRepository.findByDeviceId(deviceId)
                .orElseThrow(MobilePhoneNotFoundException::new);
        mobilePhone.setId(result.getId());
        return mobilePhoneRepository.save(mobilePhone);
    }
}
