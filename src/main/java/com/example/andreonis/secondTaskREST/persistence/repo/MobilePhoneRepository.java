package com.example.andreonis.secondTaskREST.persistence.repo;

import com.example.andreonis.secondTaskREST.persistence.model.MobilePhone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MobilePhoneRepository extends CrudRepository<MobilePhone, Long> {
    Optional<MobilePhone> findByDeviceId(String deviceId);
    void deleteByDeviceId(String deviceId);
}
