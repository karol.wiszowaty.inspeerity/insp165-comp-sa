package com.example.andreonis;

import com.example.andreonis.secondTaskREST.persistence.model.MobilePhone;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MobilePhoneControllerTests {

    private static final String API_ROOT = "http://localhost:8081/api/mobilephones";

    //GET
    @Test
    public void whenGetAllMobilePhones_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    @Test
    public void whenGetMobilePhonesByDeviceIds_thenOK() {
        final MobilePhone mobilePhone = createRandomMobilePhone();
        createMobilePhoneUri(mobilePhone);

        final Response response = RestAssured.get(API_ROOT + "/" + mobilePhone.getDeviceId());
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(response.asString().getBytes()
                .length > 0);
    }

    @Test
    public void whenGetCreatedMobilePhoneByDeviceId_thenOK() {
        final MobilePhone mobilePhone = createRandomMobilePhone();
        final String location = createMobilePhoneUri(mobilePhone);

        final Response response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals(mobilePhone.getDeviceId(), response.jsonPath()
                .get("deviceId"));
    }

    @Test
    public void whenGetNotExistMobilePhoneByDeviceId_thenNotFound() {
        final Response response = RestAssured.get(API_ROOT + "/" + randomNumeric(5));
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    // POST
    @Test
    public void whenCreateNewMobilePhone_thenCreated() {
        final MobilePhone mobilePhone = createRandomMobilePhone();

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(mobilePhone)
                .post(API_ROOT);
        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
    }

    @Test
    public void whenInvalidMobilePhone_thenError() {
        final MobilePhone mobilePhone = createRandomMobilePhone();
        mobilePhone.setDeviceId(null);

        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(mobilePhone)
                .post(API_ROOT);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getStatusCode());
    }

    @Test
    public void whenUpdateCreatedMobilePhone_thenUpdated() {
        final MobilePhone mobilePhone = createRandomMobilePhone();
        final String location = createMobilePhoneUri(mobilePhone);

        mobilePhone.setDeviceId(location.split("api/mobilephones/")[1]);
        mobilePhone.setLongitude(12);
        mobilePhone.setLatitude(6);
        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(mobilePhone)
                .put(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertEquals((Integer) 12, response.jsonPath()
                .get("longitude"));
        assertEquals((Integer) 6, response.jsonPath()
                .get("latitude"));

    }

    @Test
    public void whenDeleteCreatedBook_thenOk() {
        final MobilePhone mobilePhone = createRandomMobilePhone();
        final String location = createMobilePhoneUri(mobilePhone);

        Response response = RestAssured.delete(location);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());

        response = RestAssured.get(location);
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    //helper functions

    private MobilePhone createRandomMobilePhone() {
        RandomDataGenerator randomDataGenerator = new RandomDataGenerator();
        final MobilePhone mobilePhone = new MobilePhone();
        mobilePhone.setId(randomDataGenerator.nextLong(1,1000));
        mobilePhone.setDeviceId(randomAlphabetic(10));
        mobilePhone.setLatitude(randomDataGenerator.nextInt(1,1000));
        mobilePhone.setLongitude(randomDataGenerator.nextInt(1,1000));
        return mobilePhone;
    }

    private String createMobilePhoneUri(MobilePhone mobilePhone) {
        final Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(mobilePhone)
                .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath()
                .get("deviceId");
    }
}
