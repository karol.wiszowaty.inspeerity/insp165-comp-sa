package com.example.andreonis;

import com.stripe.model.*;
import com.stripe.param.CustomerListParams;
import com.stripe.param.InvoiceItemListParams;
import com.stripe.param.PriceListParams;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
public class InvoicesControllerTests {

    private static final String API_ROOT = "http://localhost:8081/api/invoices";

    //GET
    @Test
    public void whenGetAllInvoices_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }


    @Test
    public void whenCreateProduct_andCreateMonthlyPriceInPLNForThisProduct_thenOK(){
        final Response productResponse = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .pathParam("productName", "My REST API Test Product")
                .post(API_ROOT + "/product/{productName}");
        assertEquals(HttpStatus.CREATED.value(), productResponse.getStatusCode());

        final Response priceResponse = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParams("productId", productResponse.jsonPath().get("id"), "interval", "month", "unitAmount", 4000, "currency", "PLN")
                .post(API_ROOT + "/price");
        assertEquals(HttpStatus.CREATED.value(), priceResponse.getStatusCode());
    }

    @Test
    public void whenCreateCustomer_thenOK(){
        final Response customerResponse = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParams("nameAndSurname", "Jan Kowalski", "email", "jan.kowalski@harnas.pl", "description", "This is a user created by REST API Test")
                .post(API_ROOT + "/customer");
        assertEquals(HttpStatus.CREATED.value(), customerResponse.getStatusCode());
    }

    @Test
    public void whenCreateInvoiceItem_thenOK() throws Exception {
        //getting random customer and price that is one_time type
        Customer customer = Customer.list(CustomerListParams.builder().build()).getData()
                .stream()
                .findAny()
                .orElseThrow(() -> new Exception("There is no customer available, create one and try again later..."));
        Price price = Price.list(PriceListParams.builder().setType(PriceListParams.Type.ONE_TIME).build()).getData()
                .stream()
                .findAny()
                .orElseThrow(() -> new Exception("There is no price available, create one and try again later..."));

        final Response invoiceItemResponse = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParams("customerId", customer.getId(), "priceId", price.getId())
                .post(API_ROOT + "/invoiceItem");
        assertEquals(HttpStatus.CREATED.value(), invoiceItemResponse.getStatusCode());
    }

    @Test
    public void whenCreateInvoice_thenOK() throws Exception {
        InvoiceItem invoiceItem = InvoiceItem.list(InvoiceItemListParams.builder().build()).getData()
                .stream()
                .findAny()
                .orElseThrow(() -> new Exception("There is no invoice item available, create one and try again later..."));
        //getting random customer
        Customer customer = Customer.retrieve(invoiceItem.getCustomer());
        final Response invoiceResponse = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .queryParams("customerId", customer.getId(), "setAutoAdvance", true /* Auto-finalize this draft after ~1 hour*/, "collectionMethodType", "charge_automatically")
                .post(API_ROOT + "/invoice");
        assertEquals(HttpStatus.CREATED.value(), invoiceResponse.getStatusCode());
    }

    //based on what we want to achieve and on our resources we can delete the output of operations after tests end
}
