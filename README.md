#Zadania w celu potwierdzenia deklarowanych umiejętności

1. Zaimplementować w języku Java abstrakcję klas drzewa liściastego i iglastego (chodzi model OOP zwykłych drzew występujących w środowisku naturalnym) - każde drzewo ma pień, gałęzie i liście oraz metodę rośnij i itp.
2. Zaimplementować wydajną usługę REST, której zadaniem będzie odbieranie i zapisywanie informacji o pozycjach (geolokalizacji) z urządzeń mobilnych np. telefon/lokalizator GPS. Przykład formatu danych z urządzeń: {'deviceId':'12345', 'latitiude': 505430, 'longitude': 1423412}.
3. Zaimplementować bibliotekę kliencką obsługi faktur w systemie płatności Stripe (https://stripe.com/docs/api/invoices). W ramach zadania należy obsłużyć tworzenie i pobieranie faktur.



Wymagania implementacyjne:
- opis rozwiązania w projekcie w pliku README.md,
- testy jednostkowe rozwiązania,
- obsługa logowania,
- walidacja parametrów,
- wykorzystanie spring boota 2.


###Zadanie 1
Do rozwiązania podszedłem aby jak najbardziej umożliwić przyszłościowe
rozbudowanie aplikacji. Użycie obu - interfejsu oraz abstrakcyjnych klas bazowych umożliwia
bardziej elastyczną formę kodu. Dzięki takiemu podejściu jest możliwość zaimplementowania
prostych klas drzew poprzez "_subowanie_" bazowych klas abstrakcyjnych bądź jeżeli chcemy
osiągnąć coś bardziej zaawansowanego możemy zaimplementować kolejne drzewa bezprośrednio 
używając interfejsu **Tree** bez dziedziczenia klas bazowych. Takie podejście obrazuje
również bardzo dobrze hierarchię klas.

###Zadanie 2

REST został zaimplementowany zgodnie ze standardową implementacją Spring Boot'ową.
Dzięki Spring Boot'owi jesteśmy w stanie pozbyć się wielu konfiguracji które to są
przetwarzane automatycznie a sam kod staje się łatwiejszy do czytania dzięki pozbyciu się
"boiler plate" kodu. Aby jeszcze bardziej zniwelizować niepotrzebne linnie kodu
dodałem bibliotekę lombok która wspomaga ten proces. Encje niestety ale wymagają ręcznego
napisania metod HashCode oraz Equals by te działały wydajnie i bezbłędnie z kolekcjami 
bazującymi na hashcode. Użyłem również interfejsu CrudRepository<T> w celu skrócenia
czasu developmentu. W połączeniu z funkcjonalnością SpEL która daje nam możliwość
tworzenia "querek" jako nazwa funkcji development jest dużo szybszy i sam kod staje się
łatwiejszy do czytania. Przykłady są w MobilePhoneRepository. Wszystkie wyjątki związane z REST'em
zostały i w przyszłości będą implementowane przy użyciu ExceptionHandlera który zawiera
się nad klasą oznaczoną jako AdviceController. Pozwala to na czytelne oraz wygodne
umiejscowienie kodu związanego z samymi wyjątkami. Jako magazyn elementów służy H2.

###Zadanie 3

W celu zarządzania zewnętrznego API utworzony został serwis **_InvoicesService_**
w którym to zawiera się cała logika funkcjonalności API. W następnej kolejności stworzony
został Controller za pomocą którego wystawiamy nasze wewnętrzne API przy pomocy którego
będziemy mogli wykonywać operacje na API zewnętrznym (_TO\_IMPLEMENT_). Klasa zawierająca
konfiguracje dodaje nasz Stripe.apiKey przy inicjalizacji kontekstu tak abyśmy mieli dostęp
do API zewnętrznego w każdej chwili. Testy zostały napisane w celu testowania naszego API, nie mniej
jednak możemy również napisać testy dla API zewnętrznego. Testy mogą zawierać wiele uspewnień
zależne od tego, co tak naprawdę chcemy osiągnąć (_czyt. komentarze w klasie zawierającej testy_).
